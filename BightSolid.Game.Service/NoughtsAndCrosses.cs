﻿using System;
using System.Linq;

namespace BightSolid.Game.Service
{
    public class NoughtsAndCrosses
    {

        public NoughtsAndCrosses()
        {
            Finished = false;
            Winner = PlaceType.Empty;
        }

        public bool Finished { get;  private set; }

        public void New()
        {
            Board = new []{new PlaceType[3], new PlaceType[3], new PlaceType[3]};
        }

        // Not a 2D array as this is easier to test
        public PlaceType[][] Board { get; private set; }

        public PlaceType Winner { get; private set; }

        public void Place(int x, int y, PlaceType placeType)
        {
            if (placeType == PlaceType.Empty) throw new InvalidOperationException();
            if (Board[x][y] != PlaceType.Empty) throw new InvalidOperationException();

            Board[x][y] = placeType;
            CheckBoard();
        }

        // I really don't like this method
        // it should be done functionaly and give more time
        // it would be but because it's 100% unit tested I'm 
        // happy(ish) to ship it as stands
        private void CheckBoard()
        {
            for (var i = 0; i <= 2; i++)
            {
                var xStart = Board[i][0];
                if (xStart != PlaceType.Empty)
                {
                    if (Board[i][1] == xStart && Board[i][2] == xStart)
                    {
                        SetWinner(xStart);
                        return;
                    }
                }

                var yStart = Board[0][i];
                if (yStart != PlaceType.Empty)
                {
                    if (Board[1][i] == yStart && Board[2][i] == yStart)
                    {
                        SetWinner(yStart);
                        return;
                    }
                }
            }

            if (Board[0][0] != PlaceType.Empty)
            {
                if (Board[0][0] == Board[1][1] && Board[0][0] == Board[2][2])
                {
                    SetWinner(Board[0][0]);
                    return;
                }
            }

            if (Board[0][2] != PlaceType.Empty)
            {
                if (Board[0][2] == Board[1][1] && Board[0][2] == Board[2][0])
                {
                    SetWinner(Board[0][2]);
                }
            }

            CheckBoardFull();
        }

        private void CheckBoardFull()
        {
            for (var x = 0; x <= 2; x++)
            {
                if (Board[x].Any(p => p == PlaceType.Empty)) return;
            }

            Finished = true;
        }

        private void SetWinner(PlaceType placeType)
        {
            Finished = true;
            Winner = placeType;
        }
    }
}