﻿using System;

namespace BightSolid.Game.Service
{
    public class Player
    {
        private readonly NoughtsAndCrosses _game;
        private readonly PlaceType _oponentType;

        public Player(PlaceType placeType, NoughtsAndCrosses game)
        {
            if(placeType == PlaceType.Empty) throw new InvalidOperationException("Players PlaceType can not be PlaceType.Empty");

            _game = game;

            PlaceType = placeType;
            LastMove = new int[2];
            LastMove[0] = -1;
            LastMove[1] = -1;

            if (placeType == PlaceType.Cross) _oponentType = PlaceType.Nought;
            if (placeType == PlaceType.Nought) _oponentType = PlaceType.Cross;
        }

        public PlaceType PlaceType { get; private set; }

        public int[] LastMove { get; private set; }

        public void Move()
        {
            if (_game.Finished) throw new InvalidOperationException();
            if (Defend()) return;
            GoCrazy();
        }

        private void GoCrazy()
        {
            var randomGenerator = new Random();
            var moved = false;

            while (!moved)
            {
                var x = randomGenerator.Next(0, 2);
                var y = randomGenerator.Next(0, 2);
                if (_game.Board[x][y] == PlaceType.Empty) moved = SetPlace(x, y);
            }
        }

        private bool Defend()
        {
            if (CheckXAxis()) return true;
            if (CheckYAxis()) return true;
            if (CheckLeftDiagonal()) return true;
            if (CheckRightDiagonal()) return true;
            return false;
        }

        // this and Check Left are structurally the same and could be
        // the same function called through delegation but for this
        // I'll live with it.
        private bool CheckRightDiagonal()
        {
            var arrayToCheck = new PlaceType[] { _game.Board[0][2], _game.Board[1][1], _game.Board[2][0] };
            var index = CheckArray(arrayToCheck);

            if (index == -1) return false;
            if (index == 0) return SetPlace(0, 2);
            if (index == 1) return SetPlace(1, 1);
            if (index == 2) return SetPlace(2, 0);

            return false;
        }

        private bool CheckLeftDiagonal()
        {
            var arrayToCheck = new PlaceType[] { _game.Board[0][0], _game.Board[1][1], _game.Board[2][2] };
            var index = CheckArray(arrayToCheck);

            if (index == -1) return false;
            if (index == 0) return SetPlace(0, 0);
            if (index == 1) return SetPlace(1, 1);
            if (index == 2) return SetPlace(2, 2);

            return false;
        }

        // this and Check X are structurally the same and could be
        // the same function call through delegation but for this 
        // I'll live with it, again.
        private bool CheckYAxis()
        {
            for (int x = 0; x <= 2; x++)
            {
                var arrayToCheck = new PlaceType[] { _game.Board[x][0], _game.Board[x][1], _game.Board[x][2] };
                var indexToSet = CheckArray(arrayToCheck);
                if (indexToSet != -1 && _game.Board[x][indexToSet] == PlaceType.Empty) return SetPlace(x, indexToSet);
            }

            return false;
        }

        private bool CheckXAxis()
        {
            for (int y = 0; y <= 2; y++)
            {
                var arrayToCheck = new PlaceType[] { _game.Board[0][y], _game.Board[1][y], _game.Board[2][y] };
                var indexToSet = CheckArray(arrayToCheck);
                if (indexToSet != -1 && _game.Board[indexToSet][y] == PlaceType.Empty) return SetPlace(indexToSet, y);
            }

            return false;
        }

        private int CheckArray(PlaceType[] arrayToCheck)
        {
            if (arrayToCheck[1] == _oponentType && arrayToCheck[2] == _oponentType) return 0;
            if (arrayToCheck[0] == _oponentType && arrayToCheck[2] == _oponentType) return 1;
            if (arrayToCheck[0] == _oponentType && arrayToCheck[1] == _oponentType) return 2;

            return -1;
        }

        private bool SetPlace(int x, int y)
        {
            _game.Place(x, y, PlaceType);
            LastMove[0] = x;
            LastMove[1] = y;
            return true;
        }
    }
}