﻿using System;
using BightSolid.Game.Service;
using NUnit.Framework;
using System.Linq;

namespace BrightSolid.Game.Tests
{
    [TestFixture]
    public class GameTests
    {
        private NoughtsAndCrosses _noughtsAndCrosses;

        [SetUp]
        public void SetUp()
        {
            _noughtsAndCrosses = new NoughtsAndCrosses();
            _noughtsAndCrosses.New();
        }

        [Test]
        public void CanCreateNewGame()
        {
            Assert.That(_noughtsAndCrosses.Board.Count() == 3 &&
                        _noughtsAndCrosses.Board[0].Count() == 3 &&
                        _noughtsAndCrosses.Board[1].Count() == 3 &&
                        _noughtsAndCrosses.Board[2].Count() == 3);
        }

        [Test]
        public void NewBoardIsEmpty()
        {
            Assert.That(_noughtsAndCrosses.Board[0][0] == PlaceType.Empty &&
                        _noughtsAndCrosses.Board[0][1] == PlaceType.Empty &&
                        _noughtsAndCrosses.Board[0][2] == PlaceType.Empty &&
                        _noughtsAndCrosses.Board[1][0] == PlaceType.Empty &&
                        _noughtsAndCrosses.Board[1][1] == PlaceType.Empty &&
                        _noughtsAndCrosses.Board[1][2] == PlaceType.Empty &&
                        _noughtsAndCrosses.Board[2][0] == PlaceType.Empty &&
                        _noughtsAndCrosses.Board[2][1] == PlaceType.Empty &&
                        _noughtsAndCrosses.Board[2][2] == PlaceType.Empty);
        }

        [TestCase(0, 0)]
        [TestCase(0, 1)]
        [TestCase(0, 2)]
        [TestCase(1, 0)]
        [TestCase(1, 1)]
        [TestCase(1, 2)]
        [TestCase(2, 0)]
        [TestCase(2, 1)]
        [TestCase(2, 2)]
        public void CanPlaceOnBoard(int x, int y)
        {
            _noughtsAndCrosses.Place(x, y, PlaceType.Cross);

            Assert.That(_noughtsAndCrosses.Board[x][y] == PlaceType.Cross);
        }

        [Test, ExpectedException(typeof (InvalidOperationException))]
        public void CantPlaceOnFullBoard()
        {
            CreateStaleMate();

            _noughtsAndCrosses.Place(0,0, PlaceType.Cross);
        }

        [Test, ExpectedException(typeof (InvalidOperationException))]
        public void CantPlaceEmpty()
        {
            _noughtsAndCrosses.Place(0,0, PlaceType.Empty);
        }

        [TestCase(0, 0, PlaceType.Nought)]
        [TestCase(0, 1, PlaceType.Cross)]
        [TestCase(0, 2, PlaceType.Nought)]



        [TestCase(1,1, PlaceType.Nought)]
        [TestCase(2,1, PlaceType.Nought)]

        [TestCase(2,0, PlaceType.Cross)]
        [TestCase(2,2, PlaceType.Cross)]
        public void WinningMoveWins(int x, int y, PlaceType placeType)
        {
            CreateStaleMate();
            _noughtsAndCrosses.Board[x][y] = PlaceType.Empty;
            _noughtsAndCrosses.Place(x, y, placeType);

            Assert.That(_noughtsAndCrosses.Finished && _noughtsAndCrosses.Winner == placeType);
        }

        [TestCase(0, 0, 0, 1)]
        [TestCase(1, 0, 1, 1)]
        [TestCase(2, 0, 2, 1)]

        [TestCase(0, 0, 1, 0)]
        [TestCase(0, 1, 1, 1)]
        [TestCase(0, 2, 1, 2)]

        [TestCase(0,0, 1,1)]
        [TestCase(0,2, 1,1)]
        public void EmptyStartOfAxisStopsCheck(int startX, int startY, int otherX, int otherY)
        {
            _noughtsAndCrosses.Board[startX][startY] = PlaceType.Empty;
            _noughtsAndCrosses.Board[otherX][otherY] = PlaceType.Empty;

            _noughtsAndCrosses.Place(otherX, otherY, PlaceType.Cross);

            Assert.That(!_noughtsAndCrosses.Finished);
        }

        [Test]
        public void FullStaleMateBoardIsFinishedButNotWon()
        {
            CreateStaleMate();
            _noughtsAndCrosses.Board[2][2] = PlaceType.Empty;

            _noughtsAndCrosses.Place(2, 2, PlaceType.Nought);

            Assert.That(_noughtsAndCrosses.Finished && _noughtsAndCrosses.Winner == PlaceType.Empty);
        }

        private void CreateStaleMate()
        {
            _noughtsAndCrosses.Board[0][0] = PlaceType.Cross;
            _noughtsAndCrosses.Board[0][1] = PlaceType.Nought;
            _noughtsAndCrosses.Board[0][2] = PlaceType.Cross;

            _noughtsAndCrosses.Board[1][0] = PlaceType.Nought;
            _noughtsAndCrosses.Board[1][1] = PlaceType.Cross;
            _noughtsAndCrosses.Board[1][2] = PlaceType.Nought;
            
            _noughtsAndCrosses.Board[2][0] = PlaceType.Nought;
            _noughtsAndCrosses.Board[2][1] = PlaceType.Cross;
            _noughtsAndCrosses.Board[2][2] = PlaceType.Nought;
        }


    }
}