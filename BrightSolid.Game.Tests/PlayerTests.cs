﻿using System;
using BightSolid.Game.Service;
using NUnit.Framework;

namespace BrightSolid.Game.Tests
{
    [TestFixture]
    public class PlayerTests
    {
        private Player _player;
        private NoughtsAndCrosses _noughtsAndCrosses;

        [SetUp]
        public void SetUp()
        {
            // I know this breaks unit testing and should be mocked
            // but for the sake of brevity I'm using the concrete class.
            _noughtsAndCrosses = new NoughtsAndCrosses();
            _noughtsAndCrosses.New();

            _player = new Player(PlaceType.Cross, _noughtsAndCrosses);
        }

        [Test, ExpectedException(typeof (InvalidOperationException))]
        public void CanNotSetPlaceTypeToEmpty()
        {
            new Player(PlaceType.Empty, _noughtsAndCrosses);
        }

        [Test]
        public void NewPlayerHasGivenPlaceType()
        {
            Assert.That(_player.PlaceType == PlaceType.Cross);
        }

        [Test]
        public void PlayerDefendsXAxis()
        {
            _noughtsAndCrosses.Board[0][2] = PlaceType.Nought;
            _noughtsAndCrosses.Board[2][2] = PlaceType.Nought;

            _player.Move();

            Assert.That(_noughtsAndCrosses.Board[1][2] == PlaceType.Cross);
        }

        [Test]
        public void PlayerDenfendsYAxis()
        {
            _noughtsAndCrosses.Board[2][0] = PlaceType.Nought;
            _noughtsAndCrosses.Board[2][2] = PlaceType.Nought;

            _player.Move();

            Assert.That(_noughtsAndCrosses.Board[2][1] == PlaceType.Cross);
        }

        [Test]
        public void PlayerDefendXAxisScenario2()
        {
            _noughtsAndCrosses.Board[0][1] = PlaceType.Nought;
            _noughtsAndCrosses.Board[1][1] = PlaceType.Nought;

            _player.Move();

            Assert.That(_noughtsAndCrosses.Board[2][1] == PlaceType.Cross);
        }

        [Test]
        public void PlayerDefendYAxisScenario2()
        {
            _noughtsAndCrosses.Board[1][0] = PlaceType.Nought;
            _noughtsAndCrosses.Board[1][1] = PlaceType.Nought;

            _player.Move();

            Assert.That(_noughtsAndCrosses.Board[1][2] == PlaceType.Cross);
        }

        [Test]
        public void PlayerDefendsLeftDiagonal()
        {
            _noughtsAndCrosses.Board[0][0] = PlaceType.Nought;
            _noughtsAndCrosses.Board[2][2] = PlaceType.Nought;

            _player.Move();

            Assert.That(_noughtsAndCrosses.Board[1][1] == PlaceType.Cross);

        }

        [Test]
        public void PlayerDefendsRightDiagonal()
        {
            _noughtsAndCrosses.Board[0][2] = PlaceType.Nought;
            _noughtsAndCrosses.Board[2][0] = PlaceType.Nought;

            _player.Move();

            Assert.That(_noughtsAndCrosses.Board[1][1] == PlaceType.Cross);

        }

        [Test]
        public void BeforeFirstMoveLastMoveNotSet()
        {
            Assert.That(_player.LastMove[0] == -1 &&
                        _player.LastMove[1] == -1);
        }

        [Test]
        public void WhenNothingToDefendMakesRandomMove()
        {
            _noughtsAndCrosses.Board[0][1] = PlaceType.Nought;

            _player.Move();

            Assert.That(_player.LastMove[0] != -1 &&
                        _player.LastMove[1] != -1);
        }

        [Test, ExpectedException(typeof (InvalidOperationException))]
        public void ExecptionWhenPlayerCantMove()
        {
            _noughtsAndCrosses.Board[0][0] = PlaceType.Cross;
            _noughtsAndCrosses.Board[1][0] = PlaceType.Nought;
            _noughtsAndCrosses.Board[2][0] = PlaceType.Cross;
            _noughtsAndCrosses.Board[0][1] = PlaceType.Nought;
            _noughtsAndCrosses.Board[1][1] = PlaceType.Cross;
            _noughtsAndCrosses.Board[2][1] = PlaceType.Nought;
            _noughtsAndCrosses.Board[0][2] = PlaceType.Nought;
            _noughtsAndCrosses.Board[1][2] = PlaceType.Cross;
            _noughtsAndCrosses.Board[2][2] = PlaceType.Empty;

            _noughtsAndCrosses.Place(2,2, PlaceType.Nought);

            _player.Move();
        }
    }
}