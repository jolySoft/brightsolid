﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BightSolid.Game.Service;

namespace BightSolid.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            RunGame();
        }

        private static void RunGame()
        {
            System.Console.WriteLine("Hello. Would like to watch a game of Doughs and Crosses? (Y|N)");
            var answer = System.Console.ReadKey();
            if (answer.KeyChar == 'Y' || answer.KeyChar == 'y') StartGame();
        }

        private static void StartGame()
        {
            var noughtsAndCrosses = new NoughtsAndCrosses();
            noughtsAndCrosses.New();
            System.Console.WriteLine("Board Created");
            
            var player1 = new Player(PlaceType.Cross, noughtsAndCrosses);
            System.Console.WriteLine("Player 1 Created (Crosses)");
            
            var player2 = new Player(PlaceType.Nought, noughtsAndCrosses);
            System.Console.WriteLine("Player 2 Created (Noughts)");

            while (!noughtsAndCrosses.Finished)
            {
                player1.Move();
                System.Console.WriteLine("Player 1 last move was: x-" + player1.LastMove[0] + ", y-" + player1.LastMove[1]);
                if (noughtsAndCrosses.Finished) break;

                player2.Move();
                System.Console.WriteLine("Player 2 last move was: x-" + player2.LastMove[0] + ", y-" + player2.LastMove[1]);
            }

            if (noughtsAndCrosses.Winner == PlaceType.Empty) System.Console.WriteLine("The game was a draw");
            if (noughtsAndCrosses.Winner == PlaceType.Cross) System.Console.WriteLine("The game was won by Player 1 (Crosses)");
            if (noughtsAndCrosses.Winner == PlaceType.Nought) System.Console.WriteLine("The game was won by Player 2 (Noughts)");

            System.Console.ReadKey();
        }
    }
}
